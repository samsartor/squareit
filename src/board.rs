use std::io::{Error as IoError, prelude::*};

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Square {
    Empty,
    Blocked,
    Full,
    Active,
    Portal,
}

impl Square {
    pub fn incomplete(self) -> bool {
        use self::Square::*;
        match self {
            Empty | Portal => true,
            Blocked | Full | Active  => false,
        }
    }

    pub fn from_byte(c: u8) -> Option<Square> {
        use self::Square::*;
        Some(match c {
            b' ' => Empty,
            b'#' => Blocked,
            b'X' => Full,
            b'O' => Active,
            b'.' => Portal,
            _ => return None,
        })
    }

    pub fn repr(self) -> char {
        use self::Square::*;
        match self {
            Empty => ' ',
            Blocked => '#',
            Full => 'X',
            Active => 'O',
            Portal => '.',
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Board {
    width: usize,
    height: usize,
    buf: Vec<Square>,
    pub history: Vec<Dir>,
}

#[derive(Debug, Fail)]
pub enum BoardReadError {
    #[fail(display="input error: {}", _0)]
    Io(IoError),
    #[fail(display="unknown byte: 0x{:02X}", _0)]
    UnknownByte(u8),
    #[fail(display="width is inconsistent")]
    InconsistentWidth,
}

impl From<IoError> for BoardReadError {
    fn from(e: IoError) -> BoardReadError { BoardReadError::Io(e) }
}

impl Board {
    pub fn read<R: Read>(read: R) -> Result<Board, BoardReadError> {
        let mut width = None;
        let mut height = 0;
        let mut buf = Vec::new();
        let mut travel = 0;

        for b in read.bytes() {
            let b = b?;
            match b {
                b'\n' => {
                    let width = match width {
                        None => { width = Some(travel); travel }
                        Some(w) if w != travel => return Err(BoardReadError::InconsistentWidth),
                        Some(w) => w,
                    };
                    buf.reserve(width * 2);
                    travel = 0;
                    height += 1;
                }
                b'\r' => continue,
                b => {
                    travel += 1;
                    buf.push(Square::from_byte(b).ok_or(BoardReadError::UnknownByte(b))?)
                },
            }
        }

        if travel != 0 {
            match width {
                None => width = Some(travel),
                Some(w) if w != travel => return Err(BoardReadError::InconsistentWidth),
                _ => (),
            };
            height += 1;
        }

        return Ok(Board {
            width: width.unwrap_or(0),
            height,
            buf,
            history: Vec::new(),
        })
    }

    pub fn repr(&self, into: &mut String, line_prefix: &str) {
        fn bar(s: &mut String, len: usize) {
            s.push('+');
            for _ in 0..len { s.push('-') }
            s.push('+');
        }

        *into += line_prefix;
        bar(into, self.width);
        let mut ind = 0;
        for _ in 0..self.height {
            into.push('\n');
            *into += line_prefix;
            into.push('|');
            for _ in 0..self.width {
                into.push(self.buf[ind].repr());
                ind += 1;
            }
            into.push('|');
        }
        into.push('\n');
        *into += line_prefix;
        bar(into, self.width);
    }

    pub fn indicies(&self) -> impl Iterator<Item=usize> {
        0..(self.width * self.height)
    }

    pub fn locations(&self, filter: Square) -> impl Iterator<Item=usize> + '_ {
        self.indicies().filter(move |&i| self.buf[i] == filter)
    }

    pub fn incomplete(&self) -> bool {
        self.indicies().map(|i| self.buf[i]).any(Square::incomplete)
    }

    /// Slide this board in the given direction, appending all diffs to the
    /// given change buffer.  Does not append to history.
    pub fn calculate(&self, dir: Dir, changes: &mut Vec<Change>) {
        use std::ops::AddAssign;
        use self::Square::*;

        #[derive(Copy, Clone)]
        enum PortalMode {
            /// Activate no portals
            None,
            /// Activate most portals, fill one
            Most { fill: usize },
            /// Activate all portals
            All,
        }

        impl AddAssign<usize> for PortalMode {
            fn add_assign(&mut self, fill: usize) {
                *self = match *self {
                    PortalMode::None => PortalMode::Most { fill },
                    PortalMode::Most { .. } => PortalMode::All,
                    PortalMode::All => PortalMode::All,
                };
            }
        }

        // How should portals be changed?
        let mut portal = PortalMode::None;

        // Determine changes to make
        for i in self.locations(Active) {
            // Try to move to square
            let m = match dir.move_index(i, self) {
                Some(m) => m,
                None => continue,
            };

            match self.buf[m] {
                // Fill current location and activate empty square
                Empty => changes.extend_from_slice(&[Change(i, Full), Change(m, Active)]),
                // Set portal mode and fill current location
                Portal => {
                    portal += m;
                    changes.push(Change(i, Full));
                }
                // Don't move
                Active | Blocked | Full => (),
            }
        }

        // Add portal changes, if any. Does not append to history.
        match portal {
            PortalMode::None => (),
            // Only one portal entered, fill that portal and activate rest
            PortalMode::Most { fill } => for i in self.locations(Portal) {
                if i == fill { changes.push(Change(i, Full)) }
                else { changes.push(Change(i, Active)) }
            },
            // More than one portal entered, activate all portals
            PortalMode::All => for i in self.locations(Portal) {
                changes.push(Change(i, Active))
            },
        }
    }

    /// Consume and apply all changes in the given change buffer, and return the
    /// count. Also appends to history.
    pub fn apply_changes(&mut self, changes: &mut Vec<Change>) -> usize {
        // Apply changes
        let count = changes.len();
        for Change(i, v) in changes.drain(..) {
            self.buf[i] = v;
        }
        count
    }

    /// Slide this board in the given direction, using the given change buffer
    /// as an intermediary. Returns the number changes made.
    pub fn apply(&mut self, dir: Dir, changes: &mut Vec<Change>) -> usize {
        self.calculate(dir, changes);
        self.history.push(dir);
        self.apply_changes(changes)
    }
}

pub static DIRS: &[Dir] = &[Dir::Up, Dir::Down, Dir::Left, Dir::Right];

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Dir {
    pub fn move_index(self, i: usize, b: &Board) -> Option<usize> {
        use self::Dir::*;
        match self {
            Left if i % b.width == 0 => None,
            Left => Some(i - 1),
            Right if i % b.width == b.width - 1 => None,
            Right => Some(i + 1),
            Up if i < b.width => None,
            Up => Some(i - b.width),
            Down if i >= b.width * (b.height - 1) => None,
            Down => Some(i + b.width),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct Change(usize, Square);
