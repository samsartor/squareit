extern crate clap;
#[macro_use]
extern crate failure;
extern crate crossbeam_deque;
extern crate rand;

use clap::{App, Arg};
use failure::Error;

use std::fs::File;

pub mod search;
pub mod board;
pub mod ai;

fn main() -> Result<(), Error> {
    let matches = App::new("squareit")
        .arg(Arg::with_name("INPUT")
            .long("input")
            .short("i")
            .index(1)
            .required(true))
        .arg(Arg::with_name("debug")
            .long("debug")
            .takes_value(false))
        .get_matches();

    let initial = board::Board::read(File::open(matches.value_of("INPUT").unwrap())?)?;

    if matches.is_present("debug") {
        let terminal = search::debug_random_path(initial);

        let mut buf = String::new();
        println!("Final State:");
        terminal.repr(&mut buf, "  ");
        println!("{}", buf);
    } else {
        match search::find_random_solution(initial) {
            Some(solution) => {
                let mut buf = String::new();
                solution.repr(&mut buf, "");
                println!("{}\n", buf);
                for m in solution.history {
                    println!("  - {:?}", m);
                }
            },
            None => println!("No solution."),
        }
    }

    Ok(())
}

#[test]
fn test_examples() -> Result<(), Error> {
    use std::fs::{read_dir};
    use std::path::PathBuf;
    use std::ffi::OsStr;

    let mut queue = vec![PathBuf::from("examples")];

    while let Some(dir) = queue.pop() {
        for e in read_dir(dir)? {
            let e = e?;
            let ty = e.file_type()?;
            let path = e.path();
            if ty.is_dir() { queue.push(path); continue }
            if !ty.is_file() { continue }
            let name = path.file_name()
                .and_then(OsStr::to_str)
                .unwrap();
            let ty = match name.find(|c: char| !c.is_alphabetic()) {
                Some(i) => &name[..i],
                None => name,
            };
            println!("testing \"{}\"", path.display());

            let board = board::Board::read(File::open(&path)?);
            let board = match ty {
                "error" => { assert!(board.is_err()); continue },
                _ => board?,
            };

            match ty {
                "empty" => { assert_eq!(board.indicies().count(), 0); continue },
                "trivial" => assert!(!board.incomplete()),
                _ => (),
            }

            let sol = search::find_random_solution(board);
            assert_eq!(sol.is_some(), match ty {
                "empty" => true,
                "imp" => false,
                "lvl" => true,
                "trivial" => true,
                uk => panic!("unknown type {}", uk),
            });
        }
    }

    Ok(())
}
