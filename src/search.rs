use board::{Board, Dir, Change};
use std::sync::{Arc};
use std::collections::VecDeque;

#[derive(Clone, Debug)]
struct Action {
    state: Arc<Board>,
    todo: Dir,
}

impl Action {
    fn apply(&self, changes: &mut Vec<Change>) -> Option<Board> {
        let mut board = (&*self.state).clone();
        if board.apply(self.todo, changes) == 0 {
            None
        } else {
            Some(board)
        }
    }
}

pub trait Searcher {
    fn enqueue_actions<A: IntoIterator<Item=Dir>>(&mut self, state: Board, actions: A);
    fn fetch_new_state(&mut self) -> Option<Board>;
    fn provide_terminal(&mut self, Board);
}

pub fn find_random_solution(init: Board) -> Option<Board> {
     struct Search {
        changes: Vec<Change>,
        initial: Option<Board>,
        stack: VecDeque<Action>,
        solution: Option<Board>,
    }

    impl Searcher for Search {
        fn enqueue_actions<A: IntoIterator<Item=Dir>>(&mut self, state: Board, actions: A) {
            let state = Arc::new(state);
            self.stack.extend(actions
                .into_iter()
                .map(move |todo| Action {
                    todo,
                    state: state.clone(),
                }));
        }

        fn fetch_new_state(&mut self) -> Option<Board> {
            if self.solution.is_some() { return None }
            while let Some(action) = self.stack.pop_front() {
                let state = action.apply(&mut self.changes);
                if state.is_some() { return state }
            }
            self.initial.take()
        }

        fn provide_terminal(&mut self, state: Board) {
            if !state.incomplete() {
                self.solution = Some(state);
            }
        }
    }

    let mut search = Search {
        changes: Vec::with_capacity(16),
        initial: Some(init),
        stack: VecDeque::new(),
        solution: None,
    };
    ::ai::random_games(&mut search);
    search.solution
}

pub fn find_random_solution_threaded(init: Board, threads: usize) -> Option<Board> {
    use crossbeam_deque::{lifo, Stealer, Worker};
    use rand::{thread_rng, ThreadRng, Rng};
    use std::sync::{atomic::{AtomicUsize, AtomicBool, Ordering::Relaxed}, Mutex};
    use std::thread::{spawn, sleep};
    use std::time::Duration;
    use board::DIRS;

    if !init.incomplete() { return Some(init) }

    struct Context {
        stealers: Vec<Stealer<Action>>,
        output: Mutex<Vec<Board>>,
        waiting: AtomicUsize,
        done: AtomicBool,
    }

    struct Search {
        ctx: Arc<Context>,
        worker: Worker<Action>,
        changes: Vec<Change>,
        rng: ThreadRng,
    }

    impl Search {
        fn is_done(&self) -> bool {
            self.ctx.done.load(Relaxed) ||
            self.ctx.waiting.load(Relaxed) >= self.ctx.stealers.len()
        }
    }

    impl Searcher for Search {
        fn enqueue_actions<A: IntoIterator<Item=Dir>>(&mut self, state: Board, actions: A) {
            let state = Arc::new(state);
            for todo in actions {
                self.worker.push(Action {
                    todo,
                    state: state.clone(),
                })
            }
        }

        fn fetch_new_state(&mut self) -> Option<Board> {
            if self.is_done() { return None }
            while let Some(a) = self.worker.pop() {
                let s = a.apply(&mut self.changes);
                if s.is_some() { return s }
            }
            self.ctx.waiting.fetch_add(1, Relaxed);

            let mut count = 0;
            let mut dur = Duration::from_nanos(self.rng.gen_range(50, 200));
            loop {
                if let Some(s) = self.rng
                    .choose(&self.ctx.stealers[..])
                    .unwrap()
                    .steal()
                    .and_then(|a| a.apply(&mut self.changes))
                {
                    self.ctx.waiting.fetch_sub(1, Relaxed);
                    return Some(s)
                }

                count += 1;
                if count > self.ctx.stealers.len() {
                    if self.is_done() { return None }
                    sleep(dur);
                    dur *= 2;
                }
            }
        }

        fn provide_terminal(&mut self, state: Board) {
            if !state.incomplete() {
                self.ctx.done.store(true, Relaxed);
                self.ctx.output.lock().unwrap().push(state);
            }
        }
    }

    assert!(threads > 0);
    let mut workers = Vec::with_capacity(threads);
    let mut stealers = Vec::with_capacity(threads);
    for _ in 0..threads {
        let (w, s) = lifo::<Action>();
        workers.push(w);
        stealers.push(s);
    }

    let init = Arc::new(init);
    for (w, todo) in workers.iter().cycle().zip(DIRS.iter().cloned()) {
        w.push(Action { state: init.clone(), todo });
    }

    let ctx = Arc::new(Context {
        stealers,
        output: Mutex::new(Vec::new()),
        done: AtomicBool::new(false),
        waiting: AtomicUsize::new(0),
    });
    let threads: Vec<_> = workers.into_iter().map(|worker| {
        let ctx = ctx.clone();
        spawn(move || {
            let mut search = Search {
                ctx,
                worker,
                rng: thread_rng(),
                changes: Vec::with_capacity(16),
            };
            ::ai::random_games(&mut search)
        })
    }).collect();
    for t in threads { t.join().unwrap() }

    Arc::try_unwrap(ctx)
        .map_err(|_| ()).unwrap()
        .output
        .into_inner().unwrap()
        .pop()
}

pub fn debug_random_path(init: Board) -> Board {
    struct Search {
        turn: usize,
        initial: Option<Board>,
        terminal: Option<Board>,
    }

    impl Searcher for Search {
        fn enqueue_actions<A: IntoIterator<Item=Dir>>(&mut self, state: Board, actions: A) {
            let mut buf = String::new();
            state.repr(&mut buf, "  ");
            println!("State #{}:\n{}", self.turn, buf);
            for action in actions {
                println!("  - ENQUEUE {:?}", action);
            }
            println!();
            self.turn += 1;
        }

        fn fetch_new_state(&mut self) -> Option<Board> {
            self.initial.take()
        }

        fn provide_terminal(&mut self, state: Board) {
            self.terminal = Some(state);
        }
    }

    let mut search = Search {
        turn: 0,
        initial: Some(init.clone()),
        terminal: None,
    };
    ::ai::random_games(&mut search);
    search.terminal.unwrap_or(init)
}
