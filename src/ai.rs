use rand::{Rng, thread_rng};
use search::Searcher;
use board::DIRS;

pub fn random_games<S: Searcher>(search: &mut S) {
    let mut rng = thread_rng();
    let mut changes = Vec::with_capacity(16);
    let mut dirs = Vec::with_capacity(4);

    while let Some(mut state) = search.fetch_new_state() {
        'game: loop {
            dirs.extend_from_slice(DIRS);
            rng.shuffle(&mut dirs);

            while let Some(dir) = dirs.pop() {
                state.calculate(dir, &mut changes);
                if !changes.is_empty() {
                    search.enqueue_actions(state.clone(), dirs.drain(..));
                    state.apply_changes(&mut changes);
                    state.history.push(dir);
                    continue 'game;
                }
            }

            search.provide_terminal(state);
            break 'game;
        }
    }
}
